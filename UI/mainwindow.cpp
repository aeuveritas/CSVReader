#include "UI/mainwindow.h"
#include "UI/popup.h"
#include "ui_mainwindow.h"

#include <QTableView>
#include <QStandardItemModel>
#include <QMessageBox>

#include "Utility/stringlist.h"
#include "Utility/kerror.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    qBox(new QMessageBox)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete qBox;
    delete ui;
}

void MainWindow::init(CSV::CSVManager *_pm, Popup* _popup)
{
    cm = _pm;
    popup = _popup;
}

void MainWindow::fillTable()
{
    CSV::CSV* csv = cm->getCSV();
    CSV::Data* data = csv->getData();

    QTableView *view = ui->tableView;

    size_t headerCnt = data->getHeaderCnt();
    size_t itemCnt = data->getItemCnt();
    QStandardItemModel *model = new QStandardItemModel(itemCnt,
                                                       headerCnt,
                                                       this);

    Utility::StringList& header = data->getHeaderList();
    CSV::Data::DataMap& itemMap = data->getItemMap();

    int colIdx = 0;
    for (const Utility::kString& _item : header)
    {
        CSV::Data::KsmIterator itr = itemMap.find(_item);
        Utility::StringList& items = itr->second;

        model->setHorizontalHeaderItem(colIdx,
                                       new QStandardItem(QString::fromStdString(_item)));
        int rowIdx = 0;

        for (Utility::kString& item : items)
        {
            model->setItem(rowIdx++, colIdx, new QStandardItem(QString::fromStdString(item)));
        }
        colIdx++;
    }

    Utility::kError *error = Utility::kError::getInstance();

    for (Utility::kString& err : *error)
    {
        ui->lineEditMessage->setText(QString::fromStdString(err));
    }
    error->clear();

    view->setModel(model);
    view->show();
}

void MainWindow::on_buttonParse_clicked()
{
    cm->delCSV();

    Utility::kString delimiter = ui->lineEditDelimiter->text().toStdString();

    if (delimiter.size() != 1)
    {
        ui->lineEditMessage->setText("Wrong delimiter. Default delimiter: ';'");
        ui->lineEditDelimiter->setText(";");
        delimiter.assign(";");
    }

    Utility::kString header = ui->lineEditColumns->text().toStdString();
    Utility::kString data = ui->plainTextEditData->toPlainText().toStdString();

    CSV::CSV* csv = cm->getCSV();
    csv->setTable(*delimiter.c_str(), header, data);

    fillTable();

    return;
}

void MainWindow::on_tableView_doubleClicked(const QModelIndex &index)
{
    int row = index.row();
    int col = index.column();

    CSV::CSV* csv = cm->getCSV();
    CSV::Data* data = csv->getData();

    Utility::kString& header = data->getHeader(col);
    Utility::kString& item = data->getItem(row, col);

    popup->setHeader(header);
    popup->setItem(item);

    popup->show();
}

void MainWindow::on_actionAbout_triggered()
{
    qBox->setText("CSVReader - V 0.1\n\n"
                  "Git repo: https://github.com/aeuveritas/CSVReader\n"
                  "Owner: aeuveritas@gmail.com\n"
                  "License: LGPL\n\n"
                  "This includes static Qt library.");

    qBox->show();
}

void MainWindow::on_actionInfo_triggered()
{
    qBox->setText("1. Copy headers to \'Headers\' box\n2. Copy csv data to \'Data\' box\n3. Set \'Delimiter\'\n4. Click \'Parse\'");

    qBox->show();
}
