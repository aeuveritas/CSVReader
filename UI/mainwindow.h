#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include "CSV/csvmanager.h"
#include "UI/popup.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init(CSV::CSVManager *_pm, Popup *_popup);

private:
    void fillTable();

private slots:
    void on_buttonParse_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_actionInfo_triggered();

    void on_actionAbout_triggered();

private:
    Ui::MainWindow *ui;
    CSV::CSVManager *cm;

    Popup *popup;
    QMessageBox *qBox;
};

#endif // MAINWINDOW_H
