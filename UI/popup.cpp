#include "UI/popup.h"
#include "ui_popup.h"

Popup::Popup(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Popup)
{
    ui->setupUi(this);
}

Popup::~Popup()
{
    delete ui;
}

void Popup::setHeader(std::string &text)
{
    ui->header->setText(QString::fromStdString(text));
}

void Popup::setItem(std::string &text)
{
    ui->item->setText(QString::fromStdString(text));
}
