#ifndef POPUP_H
#define POPUP_H

#include <QWidget>

namespace Ui {
class Popup;
}

class Popup : public QWidget
{
    Q_OBJECT

public:
    explicit Popup(QWidget *parent = 0);
    ~Popup();

    void setHeader(std::string& text);
    void setItem(std::string& text);

private:
    Ui::Popup *ui;
};

#endif // POPUP_H
