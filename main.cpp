#include "UI/mainwindow.h"
#include "UI/popup.h"
#include <QApplication>

#include <memory>

#include "CSV/csvmanager.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    CSV::CSVManager *pm = new CSV::CSVManager();

    Popup* popup = new Popup();

    MainWindow w;
    w.init(pm, popup);
    w.show();

    return a.exec();
}
