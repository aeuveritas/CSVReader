#include "CSV/csvmanager.h"

#include <QDebug>

namespace CSV
{
CSVManager::CSVManager()
    : csv(nullptr)
{

}

CSV* CSVManager::getCSV()
{
    if (!csv)
    {
        csv = new CSV();
    }

    return csv;
}

void CSVManager::delCSV()
{
    if (csv)
    {
        delete csv;
        csv = nullptr;
    }
}
}
