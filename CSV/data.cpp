#include "CSV/data.h"
#include "Utility/kerror.h"

namespace CSV
{
Data::Data()
    : itemMap()
    , delimiter()
    , headerCnt(0)
    , itemCnt(0)
{

}

size_t Data::getHeaderCnt()
{
    return headerCnt;
}

size_t Data::getItemCnt()
{
    return itemCnt;
}

void Data::setDelimiter(const char &_delimiter)
{
    delimiter = _delimiter;
}

bool Data::setHeader(const Utility::kString &_str)
{
    std::list<Utility::kString> headers;
    _str.split(headers, delimiter);

    for (Utility::kString& header: headers)
    {
        KsmIterator itr = itemMap.find(header);
        if (itr != itemMap.end())
        {
            Utility::kError *error = Utility::kError::getInstance();
            Utility::kString err("Duplicated header: ");
            err += header;
            error->setError(err);

            headerList.clear();
            itemMap.clear();

            return false;
        }

        headerList.push_back(header);
        itemMap.insert(std::pair<Utility::kString&, Utility::StringList>(
                                    header, Utility::StringList()));
    }

    headerCnt = headerList.size();

    return true;
}

void Data::setItem(const Utility::kString &_str)
{
    Utility::StringList rowData;
    rowData.set(_str, delimiter);

    int cnt = 0;

    for (Utility::kString& item : rowData)
    {
        std::list<Utility::kString>::iterator lItr = headerList.begin();
        std::advance(lItr, cnt++);

        KsmIterator mItr = itemMap.find(*lItr);
        Utility::StringList& stringList =  mItr->second;
        stringList.push_back(item);
    }

    itemCnt++;
}

Utility::StringList& Data::getHeaderList()
{
    return headerList;
}

std::map<Utility::kString, Utility::StringList> &Data::getItemMap()
{
    return itemMap;
}

Utility::kString &Data::getHeader(int col)
{
    Utility::StringList::KslIterator itr = headerList.begin();
    std::advance(itr, col);

    return *itr;
}

Utility::kString &Data::getItem(int row, int col)
{
    Utility::StringList::KslIterator cItr = headerList.begin();
    std::advance(cItr, col);

    KsmIterator mItr = itemMap.find(*cItr);
    Utility::StringList& itemList = mItr->second;
    Utility::StringList::KslIterator rItr = itemList.begin();
    std::advance(rItr, row);

    return *rItr;
}
}
