#ifndef CSVMANAGER_H
#define CSVMANAGER_H

#include "CSV/csv.h"

namespace CSV
{
class CSVManager
{
public:
    CSVManager();

    CSV* getCSV();
    void delCSV();

private:
    CSV *csv;
};
}
#endif // CSVMANAGER_H
