#ifndef DATA_H
#define DATA_H

#include <map>
#include <list>

#include "Utility/stringlist.h"

namespace CSV
{
class Data
{
public:
    Data();
    using DataMap = std::map<Utility::kString, Utility::StringList>;
    using KsmIterator = DataMap::iterator;

    void setDelimiter(const char &_delimiter);
    bool setHeader(const Utility::kString &str);
    void setItem(const Utility::kString &str);

    Utility::StringList& getHeaderList();
    std::map<Utility::kString, Utility::StringList>& getItemMap();

    Utility::kString& getHeader(int col);
    Utility::kString& getItem(int row, int col);

    size_t getHeaderCnt();
    size_t getItemCnt();

private:
    DataMap itemMap;
    Utility::StringList headerList;
    char delimiter;

    size_t headerCnt;
    size_t itemCnt;
};
}
#endif // DATA_H
