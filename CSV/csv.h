#ifndef CSV_H
#define CSV_H

#include <QString>
#include <QStringList>

#include "Utility/kstring.h"

#include "CSV/data.h"

namespace CSV
{
class CSV
{
public:
    CSV();
    ~CSV();

    void setTable(const char& _delimiter,
                  const Utility::kString& _headers,
                  const Utility::kString& _data);
    void setDelimiter(const char& _delimiter);
    bool setHeaders(const Utility::kString& _headers);
    void setData(const Utility::kString& _data);

    inline Data* getData()
    {
        return &data;
    }

    int getDelimiterCount(const Utility::kString& data);

private:
    char delimiter;
    Data data;
};
}
#endif // CSV_H
