#include "CSV/csv.h"
#include "Utility/kerror.h"

#include <iostream>

namespace CSV
{
CSV::CSV()
    : delimiter()
    , data()
{
}

CSV::~CSV()
{
}

void CSV::setTable(const char &_delimiter,
                      const Utility::kString &_headers,
                      const Utility::kString &_item)
{
    setDelimiter(_delimiter);
    if (setHeaders(_headers))
    {
        setData(_item);
    }
}

void CSV::setDelimiter(const char& _delimiter)
{
    delimiter = _delimiter;
    data.setDelimiter(_delimiter);
}

bool CSV::setHeaders(const Utility::kString& _headers)
{
    return data.setHeader(_headers);
}

void CSV::setData(const Utility::kString& str)
{
    std::list<Utility::kString> lines;

    str.split(lines, '\n');

    Utility::kString row;

    int headerCnt = data.getHeaderCnt();
    int currentCnt = 0;
    Utility::kString temp;

    bool isContinue = false;

    for (Utility::kString& line: lines)
    {
        if (isContinue)
        {
            row = temp + '\n' + line;
        }
        else
        {
            row = line;
        }

        currentCnt = row.getDelimiterCount(delimiter);

        if (headerCnt == currentCnt)
        {
            data.setItem(row);
            isContinue = false;
        }
        else if (headerCnt > currentCnt)
        {
            temp = row;
            isContinue = true;
        }
        else
        {
            Utility::kError *error = Utility::kError::getInstance();
            Utility::kString err("Wrong data format");
            error->setError(err);

            isContinue = false;
        }
    }
}
}
