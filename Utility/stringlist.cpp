#include "Utility/stringlist.h"

#include <iostream>
#include <sstream>

namespace Utility {
StringList::StringList()
    : items()
{

}

StringList::~StringList()
{

}

void StringList::set(const Utility::kString &data, const char& delimiter)
{
    items.clear();

    append(data, delimiter);
}

void StringList::append(const Utility::kString &data, const char& delimiter)
{
    std::list<Utility::kString> itemList;

    data.split(itemList, delimiter);

    for (Utility::kString item : itemList)
    {
        items.push_back(item);
    }
}

void StringList::print()
{
    for (const Utility::kString& str : items)
    {
        std::cout << str << " ";
    }
    std::cout << std::endl;
}

}
