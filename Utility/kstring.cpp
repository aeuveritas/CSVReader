#include "Utility/kstring.h"
#include <algorithm>

Utility::kString::kString()
    : std::string()
{

}

Utility::kString::kString(std::string& str)
    : std::string(str)
{}

Utility::kString::kString(const std::string &str)
    : std::string(str)
{

}

void Utility::kString::split(std::list<Utility::kString> &strList, const char &delimiter) const
{
    kString str(*this);
    size_t cutPnt;

    while ((cutPnt = str.find_first_of(delimiter)) != str.npos)
    {
        if (cutPnt > 0)
        {
            strList.push_back(str.substr(0, cutPnt));
        }
        else if (cutPnt == 0)
        {
            strList.push_back(std::string("(NULL)"));
        }
        str = str.substr(cutPnt+1);
    }

    if (str.length() > 0)
    {
        strList.push_back(str.substr(0, cutPnt));
    }
}

size_t Utility::kString::getDelimiterCount(const char &delimiter) const
{
    return std::count(this->begin(), this->end(), delimiter) + 1;
}
