#ifndef ERROR_H
#define ERROR_H

#include "Utility/stringlist.h"

namespace Utility
{
class kError
{
public:
    void setError(Utility::kString& err);
    void clear();

    Utility::StringList::KslIterator begin();
    Utility::StringList::KslIterator end();
\
    static kError *getInstance();

protected:
    kError();

private:
    static kError *instance;

    Utility::StringList errorList;
};
}

#endif // ERROR_H
