#ifndef STRINGLIST_H
#define STRINGLIST_H

#include <string>
#include <list>

#include "Utility/kstring.h"

namespace Utility {

class StringList
{
public:
    StringList();
    ~StringList();

    using KslIterator = std::list<Utility::kString>::iterator;

    inline void push_back(Utility::kString& str)
    {
        items.push_back(str);
    }

    inline int size()
    {
        return items.size();
    }

    inline KslIterator begin()
    {
        return items.begin();
    }

    inline KslIterator end()
    {
        return items.end();
    }

    inline void clear()
    {
        items.clear();
    }

    void set(const Utility::kString& data, const char& delimiter);
    void append(const Utility::kString& data, const char& delimiter);

    void print();

private:

    std::list<Utility::kString> items;
};

}
#endif // STRINGLIST_H
