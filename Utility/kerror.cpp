#include "kerror.h"

namespace Utility
{

kError *kError::instance = nullptr;

kError::kError():
    errorList()
{

}

void kError::setError(Utility::kString &err)
{
    errorList.push_back(err);
}

Utility::StringList::KslIterator kError::begin()
{
    return errorList.begin();
}

Utility::StringList::KslIterator kError::end()
{
    return errorList.end();
}

kError *kError::getInstance()
{
    if (instance == nullptr)
    {
        instance = new kError();
    }

    return instance;
}

void kError::clear()
{
    errorList.clear();
}

}
