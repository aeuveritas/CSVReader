#ifndef KSTRING_H
#define KSTRING_H

#include <string>
#include <list>

namespace Utility
{
class kString : public std::string
{
public:
    kString();
    kString(std::string& str);
    kString(const std::string& str);

    void split(std::list<kString>& strList, const char& delimiter) const;
    size_t getDelimiterCount(const char & delimiter) const;
};
}

#endif // KSTRING_H
